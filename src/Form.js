import React,{useEffect, useState} from 'react';
import styled from '@emotion/styled';
import useCoin from './Hooks/useCoin';
import useCrypto from './Hooks/useCrypto';
import Axios from 'axios';
import Error from './Error';

const Button = styled.input`
        margin-top:20px;
        background-color: #0c1f3ce3;
        font-weight:bold;
        font-size:20px;
        padding:10px;
       /* background-color:#66a2fe;*/
        border:none;
        width:100%;
        border-radius:10px;
        color:#FFF;
        transition:background-color .3s ease;
        &:hover {
            background-color:#326AC0;
            cursor: pointer;
        }

`
const APKEY='591a7f05ef313a68df1805f68349bda211c4739b8dd1c1849369382b10f0b6cd';
const Form = ({updateCoin, updateCoinCrypto})=> {
 
 const [ListCrypto, updateListCrypto] = useState([]);

const [error, updateError ]=useState(false);

//construyo las opciones del select y se las paso al hook
const COINS =[
    {codigo: 'ARS', nombre: 'Peso'},
    {codigo: 'USD', nombre: 'Dolar '},
    {codigo: 'EUR', nombre: 'Euro'}
  
]

//puedo usar los mismos nombres o diferentes siempre que repete la ubicacion
const [coin, OpSelection, updateState] = useCoin('Elegi una Moneda','',COINS);
//genero un segundo hook para listar las cryptos
const [Crypto, OpSelectionCrypto] = useCrypto('Elegi una Crypto Moneda','',ListCrypto);
  

//con el useEffect me aseguro de ejecutar el codigo al inicio

useEffect (()=> {

    const SearchApi = async () => {
          const url= 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=100&tsym=USD&api_key=${APKEY}';
          const result = await Axios.get (url);


          updateListCrypto(result.data.Data);
    }
    SearchApi()
},[]);


const PriceCoin =(e) => {
e.preventDefault()

//Valida datos seleccionados
if (coin==='' || Crypto==='' ) {

    updateError(true);
    return;

} else {
    updateError(false)
    updateCoin(coin)
    updateCoinCrypto(Crypto)
}

 



}

return (
    
        <form
        onSubmit={PriceCoin}>
         
         {error ? <Error mensaje="Tenes que seleccionar las monedas"/>:null}
            <OpSelection/>
            
            <OpSelectionCrypto/>
            
            <Button
                type="submit"
                value="Calcular"
            />
         

        </form>
    )
}
export default Form;