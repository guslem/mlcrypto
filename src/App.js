import React, {useState, useEffect} from 'react';
import Form from './Form';
import imagen from   './portada9.jpg';
import styled from '@emotion/styled';
import Axios from 'axios';
import PriceResult from './PriceResult';
import Spinner from './Spinner';



const Container = styled.div`
    
    max-width:900px;
    padding-left:20%;
    @media (min-width:992px){
        display:grid;
        grid-template-columns: repeat(2,1fr);
        columns-gap: 2rem;
    }
`

const Imagen = styled.img`
      max-width:100%;
      max-height:200%;
      margin-top:5rem;
      
`

const DivFondo = styled.div`
  background: url(${imagen} ); 
    background-repeat:none;
    margin:auto;
    max-width:1300px;
    max-height:9000px;
    padding-bottom:5px;
   
`;

const Heading = styled.h1`
      font-family: 'bebas neue', cursive;
      color: #3c3c52;
      text-align:left;
      font-weight:700;
      font-size:50px;
      margin-bottom:50px;
      margin-top:80px;
      padding-right:1rem;
      &::after {
        content:'';
        width:100px;
        height:6px;
        background-color: #fe8d66;
        /*background-color:#66a2fe;*/
        display:block;
}

`

const ContainerDiv = styled.div`
     color: #edf5f5;
    background-color: #08109c94;
    padding-left: 33px;
    margin-top: 16.5rem;
    font-weight: bold;
    padding: 15px 15px 15px;
`;
function App() {

  //genero los state iniciales de moneda y cripto

  const [coin, updateCoin] = useState('');
  const [coinCrypto, updateCoinCrypto] = useState('');
  const [result, updateResult] = useState({});
  const [ShowSp, updateShow] = useState(false);

  //evaluo si cambio de valor

  useEffect (() => {
   const PriceCrypto = async () => {
        if (coin === '')  return;
      
        const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${coinCrypto}&tsyms=${coin}`
  
        const resultApi = await Axios.get(url);
  
     //   updateResult(resultApi.data.DISPLAY[coinCrypto][coin]);
       updateShow(true);

        setTimeout(() => {
            updateShow(false);
            updateResult(resultApi.data.DISPLAY[coinCrypto][coin]);
           },2000)

         
        
      
      }
        PriceCrypto();
   
  
  },[coin,coinCrypto]);

  const ShowSpinner = (ShowSp) ? <Spinner/> : <PriceResult  result={result}/>

  return (
    <DivFondo>
        <Container>

              <div>
                  <Heading>Cotizador de Cryptos online</Heading>
                  <Form
                  updateCoin={updateCoin}
                  updateCoinCrypto={updateCoinCrypto}
                  />
             </div>
             <div>

                {ShowSpinner}
              
             </div>
      
        </Container>
    </DivFondo>
  );
}

export default App;
