import React,{Fragment, useState} from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
font-family: 'Bebas Neue', cursive;
color: #0e0146;
text-transform: uppercase;
font-weight:bold;
font-size:35px;
margin-top:2rem;
display:block;
background-color: #087477a1;
text-align:center;
`

const Select = styled.select`
width:100%;
display:block;
padding:0.5rem;
--webkit-appearance:none;
border-radius:1%;
font-size: larger;
    font-weight: bolder;
    color: unset;
`

//criptocompare


/*
https://min-api.cryptocompare.com/data/pricemulti?fsyms=ETH,DASH&tsyms=BTC,USD,EUR&api_key=INSERT-YOUR-API-KEY-HERE
*/



const useCrypto = (label, stateInicial, optionsCoins) => {
//creamos el state del custom hook

//console.log(optionsCoins)
const [state, updateState] = useState(stateInicial);

//al usar () se indica que el return esta implicio
const optSelectionCrypto = () => {
    return (
            <Fragment>
                {/* le paso el label por parametro */}
                <Label>{label}</Label>
                {/*  obtengo el valor del select y actualizo el state */}
                    <Select
                    onChange={e => updateState(e.target.value)} 
                    value={state}>
                        <option value="">-Seleccione-</option>
                        {/* mapeo las monedas definidas */}
                        {optionsCoins.map(option => (
                            <option key={option.CoinInfo.Id}
                        value={option.CoinInfo.Name}>{option.CoinInfo.FullName}</option>
                        ))
                        }
                     
                    </Select>
            </Fragment>
    );
}
    return [state, optSelectionCrypto, updateState];
}

export default useCrypto;