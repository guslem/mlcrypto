import React,{Fragment, useState} from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
font-family: 'Bebas Neue', cursive;
color: #0e0146;
text-transform: uppercase;
font-weight:bold;
font-size:35px;
margin-top:2rem;
display:block;
background-color: #087477a1;
text-align:center;
`

const Select = styled.select`
width:100%;
display:block;
padding:1rem;
--webkit-appearance:none;
border-radius:1%;
font-size: larger;
    font-weight: bolder;
    color: unset;
`
const useCoin = (label, stateInicial, optionsCoins) => {
//creamos el state del custom hook

const [state, updateState] = useState(stateInicial);

//al usar () se indica que el return esta implicio
const optSelection = () => {
    return (
            <Fragment>
                {/* le paso el label por parametro */}
                <Label>{label}</Label>
                {/*  obtengo el valor del select y actualizo el state */}
                    <Select
                    onChange={e => updateState(e.target.value)} 
                    value={state}>
                        <option value="">-Seleccione-</option>
                        {/* mapeo las monedas definidas */}
                        {optionsCoins.map(option => (
                            <option key={option.codigo}
                        value={option.codigo}>{option.nombre}</option>
                        ))
                        }
                     
                    </Select>
            </Fragment>
    );
}
    return [state, optSelection, updateState];
}

export default useCoin;