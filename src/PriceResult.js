import React from 'react';
import styled from '@emotion/styled';

const ContainerDiv = styled.div`
     color: #edf5f5;
    background-color: #08109c94;
    padding-left: 33px;
    margin-top: 16.5rem;
    font-weight: bold;
    padding: 15px 15px 15px;
`;
const InfoP = styled.p`
   font-size: 22px;
   span {
       font-weight:bold;
   }
`;

const PriceP = styled.p`
 font-size: 30px;
   span {
       font-weight:bold;
   }
`;


const PriceResult = ({result, ShowSp})=> {
   if (Object.keys(result).length === 0) return null;


   return(
       <ContainerDiv>
            <PriceP>Precio actual: <span>{result.PRICE}</span></PriceP>
            <InfoP>Precio mas alto:  <span> {result.HIGH24HOUR}</span></InfoP>
            <InfoP>Precio mas bajo: <span> {result.LOWDAY}</span></InfoP>
            <InfoP>Cantidad: <span> {result.SUPPLY}</span></InfoP>
       </ContainerDiv>

);


}
export default PriceResult;