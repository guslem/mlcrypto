import React from 'react';
import styled from '@emotion/styled'

const MensajeError = styled.p`
background-color: red;
color:#FFF;
font-size:20px;
text-transform:uppercase;
text-align:center;
font-family: 'Bebas neue', 'Courier New', Courier, monospace;


`

const Error =({mensaje}) => {
    return (
        <MensajeError>{mensaje}</MensajeError>
    );
}
export default Error;